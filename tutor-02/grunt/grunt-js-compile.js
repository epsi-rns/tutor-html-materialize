module.exports = function(grunt) {
  let concatFile = 'temp/js/materialize_concat.js.map';

  // configure the tasks
  let config = {
    babel: {
      options: {
        sourceMap: false,
        plugins: [
          'transform-es2015-arrow-functions',
          'transform-es2015-block-scoping',
          'transform-es2015-classes',
          'transform-es2015-template-literals',
          'transform-es2015-object-super'
        ]
      },
      bin: {
        options: {
          sourceMap: true
        },
        files: {
          'bin/materialize.js': 'temp/js/materialize_concat.js'
        }
      },
      custom: {
        options: {
          sourceMap: true
        },
        files: {
          'bin/materialize.js': 'temp/js/materialize_custom.js'
        }
      },
      dist: {
        files: {
          'dist/js/materialize.js': 'temp/js/materialize.js'
        }
      }
    },

    //  Concat
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: [
          'js/cash.js',
          'js/component.js',
          'js/global.js',
          'js/anime.min.js',
          'js/collapsible.js',
          'js/dropdown.js',
          'js/modal.js',
          'js/materialbox.js',
          'js/parallax.js',
          'js/tabs.js',
          'js/tooltip.js',
          'js/waves.js',
          'js/toasts.js',
          'js/sidenav.js',
          'js/scrollspy.js',
          'js/autocomplete.js',
          'js/forms.js',
          'js/slider.js',
          'js/cards.js',
          'js/chips.js',
          'js/pushpin.js',
          'js/buttons.js',
          'js/datepicker.js',
          'js/timepicker.js',
          'js/characterCounter.js',
          'js/carousel.js',
          'js/tapTarget.js',
          'js/select.js',
          'js/range.js'
        ],
        // the location of the resulting JS file
        dest: 'temp/js/materialize.js'
      },
      temp: {
        // the files to concatenate
        options: {
          sourceMap: true,
          sourceMapStyle: 'link'
        },
        src: [
          'js/cash.js',
          'js/component.js',
          'js/global.js',
          'js/anime.min.js',
          'js/collapsible.js',
          'js/dropdown.js',
          'js/modal.js',
          'js/materialbox.js',
          'js/parallax.js',
          'js/tabs.js',
          'js/tooltip.js',
          'js/waves.js',
          'js/toasts.js',
          'js/sidenav.js',
          'js/scrollspy.js',
          'js/autocomplete.js',
          'js/forms.js',
          'js/slider.js',
          'js/cards.js',
          'js/chips.js',
          'js/pushpin.js',
          'js/buttons.js',
          'js/datepicker.js',
          'js/timepicker.js',
          'js/characterCounter.js',
          'js/carousel.js',
          'js/tapTarget.js',
          'js/select.js',
          'js/range.js'
        ],
        // the location of the resulting JS file
        dest: 'temp/js/materialize_concat.js'
      },
      custom: {
        // the files to concatenate
        options: {
          sourceMap: true,
          sourceMapStyle: 'link'
        },
        src: [
          // must have
          'js/cash.js',
          'js/component.js',
          'js/global.js',
          'js/anime.min.js',
          // optional
          'js/dropdown.js',
          'js/sidenav.js'
        ],
        // the location of the resulting JS file
        dest: 'temp/js/materialize_custom.js'
      }
    },

    //  Uglify
    uglify: {
      options: {
        // Use these options when debugging
        // mangle: false,
        // compress: false,
        // beautify: true
      },
      dist: {
        files: {
          'dist/js/materialize.min.js': ['dist/js/materialize.js']
        }
      },
      bin: {
        files: {
          'bin/materialize.min.js': ['bin/materialize.js']
        }
      },
      extras: {
        files: {
          'extras/noUiSlider/nouislider.min.js': ['extras/noUiSlider/nouislider.js']
        }
      }
    },

    //  Compress
    compress: {
      main: {
        options: {
          archive: 'bin/materialize.zip',
          level: 6
        },
        files: [
          { expand: true, cwd: 'dist/', src: ['**/*'], dest: 'materialize/' },
          { expand: true, cwd: './', src: ['LICENSE', 'README.md'], dest: 'materialize/' }
        ]
      },

      src: {
        options: {
          archive: 'bin/materialize-src.zip',
          level: 6
        },
        files: [
          { expand: true, cwd: 'sass/', src: ['materialize.scss'], dest: 'materialize-src/sass/' },
          { expand: true, cwd: 'sass/', src: ['components/**/*'], dest: 'materialize-src/sass/' },
          {
            expand: true,
            cwd: 'js/',
            src: [
              'anime.min.js',
              'cash.js',
              'component.js',
              'global.js',
              'collapsible.js',
              'dropdown.js',
              'modal.js',
              'materialbox.js',
              'parallax.js',
              'tabs.js',
              'tooltip.js',
              'waves.js',
              'toasts.js',
              'sidenav.js',
              'scrollspy.js',
              'autocomplete.js',
              'forms.js',
              'slider.js',
              'cards.js',
              'chips.js',
              'pushpin.js',
              'buttons.js',
              'datepicker.js',
              'timepicker.js',
              'characterCounter.js',
              'carousel.js',
              'tapTarget.js',
              'select.js',
              'range.js'
            ],
            dest: 'materialize-src/js/'
          },
          { expand: true, cwd: 'dist/js/', src: ['**/*'], dest: 'materialize-src/js/bin/' },
          { expand: true, cwd: './', src: ['LICENSE', 'README.md'], dest: 'materialize-src/' }
        ]
      },

      starter_template: {
        options: {
          archive: 'templates/starter-template.zip',
          level: 6
        },
        files: [
          { expand: true, cwd: 'dist/', src: ['**/*'], dest: 'starter-template/' },
          {
            expand: true,
            cwd: 'templates/starter-template/',
            src: ['index.html', 'LICENSE'],
            dest: 'starter-template/'
          },
          {
            expand: true,
            cwd: 'templates/starter-template/css',
            src: ['style.css'],
            dest: 'starter-template/css'
          },
          {
            expand: true,
            cwd: 'templates/starter-template/js',
            src: ['init.js'],
            dest: 'starter-template/js'
          }
        ]
      },

      parallax_template: {
        options: {
          archive: 'templates/parallax-template.zip',
          level: 6
        },
        files: [
          { expand: true, cwd: 'dist/', src: ['**/*'], dest: 'parallax-template/' },
          {
            expand: true,
            cwd: 'templates/parallax-template/',
            src: ['index.html', 'LICENSE', 'background1.jpg', 'background2.jpg', 'background3.jpg'],
            dest: 'parallax-template/'
          },
          {
            expand: true,
            cwd: 'templates/parallax-template/css',
            src: ['style.css'],
            dest: 'parallax-template/css'
          },
          {
            expand: true,
            cwd: 'templates/parallax-template/js',
            src: ['init.js'],
            dest: 'parallax-template/js'
          }
        ]
      }
    },

    //  Clean
    clean: {
      temp: {
        src: ['temp/']
      }
    },

    //  Watch Files
    watch: {
      js: {
        files: ['js/**/*', '!js/init.js'],
        tasks: ['js_compile'],
        options: {
          interrupt: false,
          spawn: false
        }
      }
    },

    //  Notifications
    notify: {
      watching: {
        options: {
          enabled: true,
          message: 'Watching Files!',
          title: 'Materialize', // defaults to the name in package.json, or will use project directory's name
          success: true, // whether successful grunt executions should be notified automatically
          duration: 1 // the duration of notification in seconds, for `notify-send only
        }
      },

      js_compile: {
        options: {
          enabled: true,
          message: 'JS Compiled!',
          title: 'Materialize',
          success: true,
          duration: 1
        }
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-compress');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-concurrent');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-babel');

  // define the tasks
  grunt.registerTask('release', [
    'concat:dist',
    'babel:dist',
    'uglify:dist',
    'uglify:extras',
    'compress:main',
    'compress:src',
    'compress:starter_template',
    'compress:parallax_template',
    'clean:temp'
  ]);

  grunt.task.registerTask('configureBabel', 'configures babel options', function() {
    config.babel.bin.options.inputSourceMap = grunt.file.readJSON(concatFile);
  });

  grunt.registerTask('js_compile', ['concat:temp', 'configureBabel', 'babel:bin', 'clean:temp']);
  grunt.registerTask('monitor', ['concurrent:monitor']);
};
