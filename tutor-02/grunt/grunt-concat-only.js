module.exports = function(grunt) {
  // configure the tasks
  let config = {
    //  Concat
    concat: {
      options: {
        separator: ';'
      },
      dist: {
        // the files to concatenate
        src: [
          'js/cash.js',
          'js/component.js',
          'js/global.js',
          'js/anime.min.js',
          'js/collapsible.js',
          'js/dropdown.js',
          'js/modal.js',
          'js/materialbox.js',
          'js/parallax.js',
          'js/tabs.js',
          'js/tooltip.js',
          'js/waves.js',
          'js/toasts.js',
          'js/sidenav.js',
          'js/scrollspy.js',
          'js/autocomplete.js',
          'js/forms.js',
          'js/slider.js',
          'js/cards.js',
          'js/chips.js',
          'js/pushpin.js',
          'js/buttons.js',
          'js/datepicker.js',
          'js/timepicker.js',
          'js/characterCounter.js',
          'js/carousel.js',
          'js/tapTarget.js',
          'js/select.js',
          'js/range.js'
        ],
        // the location of the resulting JS file
        dest: 'temp/js/materialize.js'
      },
      custom: {
        // the files to concatenate
        options: {
          sourceMap: true,
          sourceMapStyle: 'link'
        },
        src: [
          'js/cash.js',
          'js/component.js',
          'js/global.js',
          'js/anime.min.js',
          'js/collapsible.js',
          'js/dropdown.js',
          'js/modal.js',
          'js/materialbox.js',
          'js/parallax.js',
          'js/tabs.js',
          'js/tooltip.js',
          'js/waves.js',
          'js/toasts.js',
          'js/sidenav.js',
          'js/scrollspy.js',
          'js/autocomplete.js',
          'js/forms.js',
          'js/slider.js',
          'js/cards.js',
          'js/chips.js',
          'js/pushpin.js',
          'js/buttons.js',
          'js/datepicker.js',
          'js/timepicker.js',
          'js/characterCounter.js',
          'js/carousel.js',
          'js/tapTarget.js',
          'js/select.js',
          'js/range.js'
        ],
        // the location of the resulting JS file
        dest: 'temp/js/materialize_custom.js'
      }
    }

  };

  grunt.initConfig(config);

  // load the tasks
  grunt.loadNpmTasks('grunt-contrib-concat');

};
