# Tutor HTML Materialize

Materialize CSS step by step, from pure html+css, to sass.

> Pure HTML5 + Materialize CSS + Custom SASS

-- -- --

## Links

### Materialize Step by Step

This repository:

* [Materialize Step by Step Repository][tutorial-materialize]

### Comparation

Comparation with other guidance:

* [Bulma Step by Step Repository][tutorial-bulma]

* [Bulma Material Design Step by Step Repository][tutorial-bulma-md]

* [Semantic UI Step by Step Repository][tutorial-semantic-ui]

### Using Materialize CSS in SSG

SSG step by step, utilizing Materialize CSS:

* [Eleventy Step by Step Repository][tutorial-11ty]

* [Hugo Step by Step Repository][tutorial-hugo]

[tutorial-11ty]:        https://gitlab.com/epsi-rns/tutor-11ty-materialize/
[tutorial-hugo]:        https://gitlab.com/epsi-rns/tutor-hugo-materialize/

[tutorial-bulma-md]:    https://gitlab.com/epsi-rns/bulma-material-design/
[tutorial-bulma]:       https://gitlab.com/epsi-rns/tutor-html-bulma
[tutorial-materialize]: https://gitlab.com/epsi-rns/tutor-html-materialize/
[tutorial-semantic-ui]: https://gitlab.com/epsi-rns/tutor-html-semantic-ui/

-- -- --

## Tutor 01

> Adding Materialize CSS

* Without Materialize CSS

* Using CDN

* Using Local

![Tutor 01][screenshot-01]

## Tutor 02

> Using Materialize CSS: Navigation Bar

* Simple

* Full Featured

* With jQuery

* Adding Material Icon

![Tutor 02][screenshot-02]

## Tutor 03

> Custom SASS

* Custom maximum width class

* Responsive gap using custom sass

![Tutor 03][screenshot-03]

## Tutor 04

> Helper Class

* Spacing Helper Demo

* Altering Variables

![Tutor 04][screenshot-04]

## Tutor 05

> HTML Box using Material Design

* Main Blog and Aside Widget

![Tutor 05][screenshot-05]

## Tutor 06

> Finishing

* Blog Post Example

![Tutor 06][screenshot-06]

-- -- --

| Disclaimer | Use it at your own risk |
| ---------- | ----------------------- |

[screenshot-01]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-01/html-materialize-preview.png
[screenshot-02]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-02/html-materialize-preview.png
[screenshot-03]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-03/html-materialize-preview.png
[screenshot-04]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-04/html-materialize-preview.png
[screenshot-05]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-05/html-materialize-preview.png
[screenshot-06]:    https://gitlab.com/epsi-rns/tutor-html-materialize/raw/master/tutor-06/html-materialize-preview.png
